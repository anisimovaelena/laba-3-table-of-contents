<?php

function table_content ($text)
{
  preg_match_all("/<h([1-6]).*?>(.*?)<\/h([1-6])>/i", $text, $matches);

  $content='<ol>';
  $depth=0;

  for ($i=0; $i < count($matches[0]); $i++){ 
      $value = $matches[0][$i];
      $value = preg_replace('/>'.$matches[2][$i].'/', ' id="title'.$i.'" > '.$matches[2][$i], $value); // ищет закрывающую > и текст заголовка, заменяет
      $text = str_replace($matches[0][$i], $value, $text);

      if (($i<>0) and ($matches[1][$i]>$matches[1][$i-1])) {
        $content.='<ol> ';
        $depth++;
      }
      elseif (($i<>0) and ($matches[1][$i]<$matches[1][$i-1]) ) {
        $quantity=$matches[1][$i-1]-$matches[1][$i];
        for ($j=1; $j<=$quantity; $j++) $content.=" </ol>";
        $depth--;
      }
      
      $content.=' <li> <a class="title-link" title="H'.$matches[1][$i].'" href="#title'.$i.'"> '.$matches[2][$i].' </a> </li>';
  

    }
  for ($j=0; $j<=$depth; $j++) $content.=" </ol>";
  
  echo $content;
  echo $text;
}


